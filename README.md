# WORDLE

a simple console **wordle** made in kotlin
**WARNING:** This is my interpretation of the game, it may not be accurate to the original one.

#### Index
- [RULES](#rules)
- [COLORATION](#coloration)
- [IMPROVEMENTS](#improvements)
- [STRATEGY](#strategy)

![screenshot](media/screenshot.png)


# RULES
The objective of the game is simple, there is a secret 5-letter word that you have to guess.
You have 6 tries and each time you must enter a word in the English alphabet, then you will get feedback on the word you've entered, that way you can know if the letter appears or not in the word and if it is in the correct position. and with those hints, it is more than enough to guess the word.

# COLORATION
To help the user understand if the letter was correct or not there is a color code in two places **game board** and **keyboard**, both have very similar color code, but it is not the same.
## Game Board
here we will get immediate feedback on the last word, without minding the last status

#### Character on place
$`\textcolor{#5c9728}{\text{GREEN}}`$ means that the letter its on his place

*Example:*
|__S U P E R__|
|-|
| **C A** $`\textcolor{#5c9728}{\text{P E}}`$ **S**

But this does not strictly mean that there are no more occurrences of that letter in the word.


*For example:*
|__G R E E N__|
|-|
|**B** $`\textcolor{#5c9728}{\text{R E}}`$ **A D**

in the word **GREEN** there are two **E** and if we try the word **BREAD** we will get painted only the one it has and that doesn't mean we don't have a second **E**

#### Character in the word

$`\textcolor{#a88b05}{\text{ORANGE}}`$ means that the letter is in the word but not in his place

*Example:*
|__R H I N O__|
|-|
|**U** $`\textcolor{#a88b05}{\text{N}}`$ $`\textcolor{#5c9728}{\text{I}}`$ $`\textcolor{#a88b05}{\text{O}}`$ **N** 

It colors the **N** and **O** because there is matching but in the **N** case only colors the first one, which means the secret word has only one **N**, this feature of coloring only the amount of letters also applies on $`\textcolor{#5c9728}{\text{GREEN}}`$


## Key Board
While the **Game Board** colors are only for the current word, the **Key Board** colors are for the whole play, which means that it is going to use all the user tries to color each key.

![Keyboard](media/keyboard.png)

#### Character discarded
When the user plays a word and a character doesn´t exist in the *secret word* the **game board** is not painting it, or leaving it white background, meanwhile the keyboard is going to paint it $`\textcolor{#f1514e}{\text{RED}}`$ from then on it will be stupid to use the character again

#### Character on place
This is a little bit different color code from the **Game Board**
it will **only** paint it $`\textcolor{#5c9728}{\text{green}}`$ if the character is in the correct place, and there are no more instances of this character on the word to be discovered.

Otherwise, it is going to paint it $`\textcolor{#a88b05}{\text{orange}}`$ even if the character is in the position. If there is one more instance of this character to be found, this will not be painted $`\textcolor{#5c9728}{\text{green}}`$

*Example: (Secret word is APPLE)*  
![AppleJapan](media/japan.png)  
As you can see, in the **game board** **P** appears green, because it is in the correct spot, but not in the **game board** meaning that there are two or more **P**

#### Character in the word

The color code for the *character in the word* is the same as in the **game board** but it can be confused with the $`\textcolor{#a88b05}{\text{orange}}`$ explained before in **character on place**, so for that reason, it is very important to always check the colors on both boards.

# IMPROVEMENTS

an idea for a future version, its to include some word-comparing algorithm like **Levenshtein** to propose words to the user if it gets stuck.




# STRATEGY
As this is a very popular game on the internet, lots of studies have been done:
- which is the best 1st word
- not guessing before 3rd try
- strange words are risky but if succeed they are rewarding

here are some videos that I found interesting

[3Blue 1Brown Solving wordle](https://www.youtube.com/v68zYyaEmEA)

[Matt parker find five words](https://youtu.be/_-AfhLQfb6w)