import java.io.File


const val INTENTOS = 6
const val LEN_PALABRA = 5

fun main(){
    val fileName = "src/main/kotlin/words.txt"//"src/main/kotlin/words/${firstChar}.txt"

    val fileLines = File(fileName).useLines { it.toList() }

    val wordsIndex= mapOf(        //index of the startting word by letter group
        'a' to 0,        'b' to 868,        'c' to 1871,        'd' to 2841,        'e' to 3576,        'f' to 3906,
        'g' to 4552,     'h' to 5237,       'i' to 5769,        'j' to 5949,        'k' to 6173,
        'l' to 6602,     'm' to 7227,       'n' to 8178,        'o' to 8646,        'p' to 8998,
        'q' to 10128,    'r' to 10231,      's' to 11026,       't' to 12692,       'u' to 13574,
        'v' to 13791,    'w' to 14075,      'x' to 14509,       'y' to 14527,       'z' to 14732,
    )


    val col= mapOf(     //TEXT COLOR
        "red"     to "\u001b[31m",
        "reset"   to "\u001b[0m",
        "black"   to "\u001b[30m",
        "green"   to "\u001b[32m",
        "yellow"  to "\u001b[33m",
        "blue"    to "\u001b[34m",
        "magenta" to "\u001b[35m",
        "cyan"    to "\u001b[36m",
        "white"   to "\u001b[97m",
    )


    val bg= mapOf(        //BACKGROUND COLOR
        "black"   to "\u001b[40m"+col.get("white"),
        "red"     to "\u001b[41m"+col.get("white"),
        "green"   to "\u001b[42m"+col.get("white"),
        "yellow"  to "\u001b[43m"+col.get("white"),
        "blue"    to  "\u001b[44m"+col.get("white"),
        "magenta" to "\u001b[45m"+col.get("black"),
        "cyan"    to "\u001b[46m"+col.get("black"),
        "white"   to "\u001b[107m"+col.get("black"),
        "reset"   to "\u001b[0m",
    ) // */


    val words = arrayOf("abuse","adult","agent","anger","apple","award","basis","beach","birth","block","blood","board","brain","bread","break","brown","buyer","cause","chain","chair","chest","chief","child","china","claim","class","clock","coach","coast","court","cover","cream","crime","cross","crowd","crown","cycle","dance","death","depth","doubt","draft","drama","dream","dress","drink","drive","earth","enemy","entry","error","event","faith","fault","field","fight","final","floor","focus","force","frame","frank","front","fruit","glass","grant","grass","green","group","guide","heart","henry","horse","hotel","house","image","index","input","issue","japan","jones","judge","knife","laura","layer","level","lewis","light","limit","lunch","major","march","match","metal","model","money","month","motor","mouth","music","night","noise","north","novel","nurse","offer","order","other","owner","panel","paper","party","peace","peter","phase","phone","piece","pilot","pitch","place","plane","plant","plate","point","pound","power","press","price","pride","prize","proof","queen","radio","range","ratio","reply","right","river","round","route","rugby","scale","scene","scope","score","sense","shape","share","sheep","sheet","shift","shirt","shock","sight","skill","sleep","smile","smith","smoke","sound","south","space","speed","spite","sport","squad","staff","stage","start","state","steam","steel","stock","stone","store","study","stuff","style","sugar","table","taste","terry","theme","thing","title","total","touch","tower","track","trade","train","trend","trial","trust","truth","uncle","union","unity","value","video","visit","voice","waste","watch","water","while","white","whole","woman","world","youth","there","where","which","whose","whoso","yours","yours","admit","adopt","agree","allow","alter","apply","argue","arise","avoid","begin","blame","break","bring","build","burst","carry","catch","cause","check","claim","clean","clear","climb","close","count","cover","cross","dance","doubt","drink","drive","enjoy","enter","exist","fight","focus","force","guess","imply","issue","judge","laugh","learn","leave","limit","marry","match","occur","offer","order","phone","place","point","press","prove","raise","reach","refer","relax","serve","shall","share","shift","shoot","sleep","solve","sound","speak","spend","split","stand","start","state","stick","study","teach","thank","think","throw","touch","train","treat","trust","visit","voice","waste","watch","worry","would","write","above","acute","alive","alone","angry","aware","awful","basic","black","blind","brave","brief","broad","brown","cheap","chief","civil","clean","clear","close","crazy","daily","dirty","early","empty","equal","exact","extra","faint","false","fifth","final","first","fresh","front","funny","giant","grand","great","green","gross","happy","harsh","heavy","human","ideal","inner","joint","large","legal","level","light","local","loose","lucky","magic","major","minor","moral","naked","nasty","naval","other","outer","plain","prime","prior","proud","quick","quiet","rapid","ready","right","roman","rough","round","royal","rural","sharp","sheer","short","silly","sixth","small","smart","solid","sorry","spare","steep","still","super","sweet","thick","third","tight","total","tough","upper","upset","urban","usual","vague","valid","vital","white","whole","wrong","young","afore","after","other","since","slash","until","where","while","aback","abaft","aboon","about","above","adown","afoot","afore","afoul","after","again","agape","agogo","agone","ahead","ahull","alike","aline","aloft","alone","along","aloof","aloud","amiss","amply","amuck","apace","apart","aptly","arear","aside","askew","awful","badly","bally","below","canny","cheap","clean","clear","coyly","daily","dimly","dirty","ditto","drily","dryly","dully","early","extra","false","fatly","feyly","first","fitly","forte","forth","fresh","fully","funny","gaily","gayly","godly","great","haply","heavy","hella","hence","hotly","icily","infra","jolly","laxly","lento","light","lowly","madly","maybe","never","newly","nobly","oddly","often","other","ought","party","piano","plain","plonk","plumb","prior","queer","quick","quite","ramen","rapid","redly","right","rough","round","sadly","sharp","sheer","shily","short","shyly","silly","since","sleek","slyly","small","sound","spang","srsly","stark","still","stone","stour","super","tally","tanto","there","thick","tight","today","tomoz","truly","twice","under","utter","verry","wanly","wetly","where","wrong","wryly","abaft","aboon","about","above","adown","afore","after","along","aloof","among","below","circa","cross","furth","minus","neath","round","since","spite","under","until","aargh","adieu","adios","alack","aloha","avast","basta","begad","bless","brava","bravo","bring","chook","damme","dildo","ditto","fudge","golly","hallo","hasta","havoc","hella","hello","howay","howdy","hullo","huzza","jesus","kapow","loose","lordy","marry","mercy","night","plonk","psych","quite","salve","skoal","sniff","sooey","there","tough","twirp","viola","vivat","wacko","wahey","whist","wirra","woops","yecch","yeesh","zowie")

    val usrTrys = Array(INTENTOS){Array(LEN_PALABRA){' '}}
    val trysStatus = Array(INTENTOS){Array(LEN_PALABRA){0}}
    var nTry = 0

    val abecedary=arrayOf('q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','ñ','z','x','c','v','b','n','m','ç')
    val keyboard : HashMap<Char, Int> = HashMap<Char, Int> ()//abecedario.map { it to 0 }.toMap()
    for(c in abecedary){
        keyboard.put(c,0)
    }


    //palabra secreta
    val secretWord = words.random()

    println(secretWord)

    var exit = false
    var exitNext = false
    var win = false

    while (!exit) {

        println("\n".repeat(50))

        //imprimir el titulo
        print("${col["blue"]}")//${back["yellow"]}")
        println("__           ______  _____  _____  _      ______ ")
        println("\\ \\         / / __ \\|  __ \\|  __ \\| |    |  ____|")
        println("  \\ \\  /\\  / / |  | | |__) | |  | | |    | |__   ")
        println("   \\ \\/  \\/ /| |  | |  _  /| |  | | |    |  __|  ")
        println("    \\  /\\  / | |__| | | \\ \\| |__| | |____| |____ ")
        println("     \\/  \\/   \\____/|_|  \\_\\_____/|______|______|")
        println()
        print(col["reset"])

        //PRINT TABLE OF PREVIOUS TRYs

        for (j in 0 until INTENTOS) {
            print(" ".repeat(16))
            for (i in 0 until LEN_PALABRA) {
                val c = arrayOf("", col.get("green"),col.get("yellow"))
                val c2 = arrayOf(bg.get("white"),bg.get("green"),bg.get("yellow"))
                print(c2[trysStatus[j][i]])
                //print("[${usrTrys[j][i].uppercase()}]")
                print(" ${usrTrys[j][i].uppercase()} ")
                print(col["reset"])
                //print(" ")
            }
            println()

        }


        //PRINT KEYBOARD
        val padding=2
        print("\n"+" ".repeat(padding))
        for(c in abecedary){
            //print(teclado.get(c))
            val colorines = arrayOf("",col.get("red"),col.get("yellow"),col.get("green"))
            val colorines2 = arrayOf(bg.get("white"),bg.get("red"),bg.get("yellow"),bg.get("green"))
            print(colorines2[keyboard.get(c)!!])
            //print("[${c.uppercase()}]")
            print(" ${c.uppercase()} ")
            print(col["reset"]+" ")

            if(c=='p'){
                print("\n"+" ".repeat(padding+1))
            }else if(c=='ñ'){
                print("\n"+" ".repeat(padding+2))
            }
        }
        println("\n")





        //COMPARE WITH SECRET WORD
        exit = exitNext
        if (!exitNext) {



            //ASK USER NEW TRY
            var isFormated = false
            var isWord = false
            var usrInput = ""

            while (!isFormated || !isWord) {

                usrInput = readln().lowercase()
                val firstChar = usrInput[0]
                isFormated = usrInput.length == LEN_PALABRA && !usrInput.contains(' ')

                if(!isFormated){
                    println("${col.get("red")}Please enter a 5 letter word${col.get("reset")}")
                }else{
                    //check if the word exists
                    for( i in wordsIndex[firstChar]!! until fileLines.size){
                        val w = fileLines[i]
                        // exists
                        if(w==usrInput){
                            isWord=true
                            break
                        }

                        //overflow to the next letter
                        if(w[0]!=firstChar){
                            isWord=false
                            break
                        }
                    }

                    if(!isWord){
                        println("${col.get("red")}Please enter an english word${col.get("reset")}")
                    }
                }

            }//ask user




            //generate a letter map of usrTry
            val occurrencesMap = mutableMapOf<Char, Int>()
            for (c in secretWord) {
                occurrencesMap.putIfAbsent(c, 0)
                occurrencesMap[c] = occurrencesMap[c]!! + 1
            }


            //first try inplace ones
            for (i in 0 until LEN_PALABRA) {
                val c = usrInput[i]
                usrTrys[nTry][i] = c

                if (c == secretWord[i]) {
                    //color grid with hits -> GREEN
                    trysStatus[nTry][i] = 1
                    occurrencesMap[c] = occurrencesMap[c]!! - 1
                }
            }

            //2nd for with hits substracted
            for (i in 0 until LEN_PALABRA) {
                val c = usrInput[i]
                usrTrys[nTry][i] = c

                //KEYBOARD
                if (c == secretWord[i]) {
                    if(occurrencesMap[c]!=null && occurrencesMap[c]!!.toInt()>0){
                        //shows keyboard in orange, you hit one but not all
                        if(keyboard.get(c)!=3){
                            keyboard.set(c,2)
                        }
                    }else{
                        //shows keyboard in green, you found all
                        keyboard.set(c,3)

                    }

                }else if(!secretWord.contains(c)){
                    //show keyboard in red if there is not that letter
                    keyboard.put(c,1)
                }

                //HIT BUT NOT INPLACE
                if(trysStatus[nTry][i]!=1){
                    if(occurrencesMap[c]!=null && occurrencesMap[c]!!.toInt()>0){
                        //color grid orange, found but not in place
                        trysStatus[nTry][i] = 2
                        occurrencesMap[c] = occurrencesMap[c]!! - 1

                        if(keyboard.get(c)!=3){
                            //show keyboard in orange also
                            keyboard.set(c,2)
                        }
                    }
                }


            }//2nd for




            nTry++

            win = usrInput == secretWord
            exitNext = win || nTry >= INTENTOS

        }//if !nextExit
    }//while

    if(!win){
        println("\nThe secret word was $secretWord")
    }else{
        println("You win")
    }

}




